const express = require("express");
const path = require("path");
const connectDB = require("./conn");
require("dotenv").config();
const app = express();
const morgan = require("morgan");
const router = express.Router();

app.use(morgan("dev"));
app.use(express.json());

const saves = require("./src/api/saves");
const auth = require("./src/api/auth");

const port = process.env.PORT || 3000;

connectDB();

router.use(express.static(path.join(__dirname, "build")));
router.use("/api/auth", auth);
router.use("/api/saves", saves);

router.get(["/", "/index"], (req, res) => {
    res.sendFile(path.join(__dirname, "build", "index.html"));
});
app.use(process.env.MODE === "home" ? "/gamewise" : "/", router);

app.listen(port, () => {
    console.log(`App listening on port ${port}`);
});
