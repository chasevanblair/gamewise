const { MongoClient } = require("mongodb");
const { Mongoose, default: mongoose } = require("mongoose");

const homeDb = "mongodb://127.0.0.1:27017/";
const serverDb =
    "mongodb://gamewise:o3sf6r6swq@192.168.171.67:27017/gamewise?authSource=admin";

const connectDB = async () => {
    try {
        mongoose.set("strictQuery", true);

        if (process.env.MODE == "production") {
            await mongoose.connect(serverDb, {
                useNewUrlParser: true,
                dbName: "gamewise",
            });
        } else {
            await mongoose.connect(homeDb, {
                useNewUrlParser: true,
                dbName: "gamewise",
            });
        }
        console.log("connected successfuly with mongoose");
    } catch (err) {
        console.error(err.message);
        process.exit(1);
    }
};

module.exports = connectDB;
