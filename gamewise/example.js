const save = require("./src/models/save");

function saveData() {
    testUser = new save({name: 'mcburgertests'});
    testUser.save((err) => {
        if (err) return console.log(err);
    })
    return 0;
}

async function loadData() {
    thisSave = await save.findOne({ name: "mcburgertests" });
    console.log(thisSave);
    return 0;
}

exports.loadData = loadData;
exports.saveData = saveData;