import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import { List, ListItemText } from "@mui/material";

export default function PlayerStats(props) {
    if (
        props.stoneAvailable &&
        props.iceAvailable &&
        props.uraniumAvailable &&
        props.mooncoalAvailable &&
        props.ironAvailable
    ) {
        return (
            <Card className="statsCard">
                <CardContent>
                    <Typography
                        sx={{ fontSize: 14 }}
                        color="text.secondary"
                        gutterBottom
                    >
                        Player Stats
                    </Typography>
                    <List>
                        <ListItemText
                            primary="Stone"
                            secondary={props.player.stone.toString()}
                        />
                        <ListItemText
                            primary="Iron"
                            secondary={props.player.iron.toString()}
                        />
                        <ListItemText
                            primary="Ice"
                            secondary={props.player.ice.toString()}
                        />
                        <ListItemText
                            primary="Moon Coal"
                            secondary={props.player.mooncoal.toString()}
                        />

                        <ListItemText
                            primary="Uranium"
                            secondary={props.player.uranium.toString()}
                        />
                    </List>
                </CardContent>
            </Card>
        );
    }

    if (
        props.stoneAvailable &&
        props.iceAvailable &&
        props.mooncoalAvailable &&
        props.ironAvailable
    ) {
        return (
            <Card className="statsCard">
                <CardContent>
                    <Typography
                        sx={{ fontSize: 14 }}
                        color="text.secondary"
                        gutterBottom
                    >
                        Player Stats
                    </Typography>
                    <List>
                        <ListItemText
                            primary="Stone"
                            secondary={props.player.stone.toString()}
                        />
                        <ListItemText
                            primary="Iron"
                            secondary={props.player.iron.toString()}
                        />
                        <ListItemText
                            primary="Ice"
                            secondary={props.player.ice.toString()}
                        />
                        <ListItemText
                            primary="Moon Coal"
                            secondary={props.player.mooncoal.toString()}
                        />
                    </List>
                </CardContent>
            </Card>
        );
    }
    if (props.stoneAvailable && props.iceAvailable && props.ironAvailable) {
        return (
            <Card className="statsCard">
                <CardContent>
                    <Typography
                        sx={{ fontSize: 14 }}
                        color="text.secondary"
                        gutterBottom
                    >
                        Player Stats
                    </Typography>
                    <List>
                        <ListItemText
                            primary="Stone"
                            secondary={props.player.stone.toString()}
                        />
                        <ListItemText
                            primary="Iron"
                            secondary={props.player.iron.toString()}
                        />
                        <ListItemText
                            primary="Ice"
                            secondary={props.player.ice.toString()}
                        />
                    </List>
                </CardContent>
            </Card>
        );
    }

    if (props.stoneAvailable && props.ironAvailable) {
        return (
            <Card className="statsCard">
                <CardContent>
                    <Typography
                        sx={{ fontSize: 14 }}
                        color="text.secondary"
                        gutterBottom
                    >
                        Player Stats
                    </Typography>
                    <List>
                        <ListItemText
                            primary="Stone"
                            secondary={props.player.stone.toString()}
                        />
                        <ListItemText
                            primary="Iron"
                            secondary={props.player.iron.toString()}
                        />
                    </List>
                </CardContent>
            </Card>
        );
    }

    if (props.stoneAvailable) {
        return (
            <Card className="statsCard">
                <CardContent>
                    <Typography
                        sx={{ fontSize: 14 }}
                        color="text.secondary"
                        gutterBottom
                    >
                        Player Stats
                    </Typography>
                    <List>
                        <ListItemText
                            primary="Stone"
                            secondary={props.player.stone.toString()}
                        />
                    </List>
                </CardContent>
            </Card>
        );
    }
}
