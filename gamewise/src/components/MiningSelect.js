import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import { Button } from "@mui/material";

export default function MiningSelect(props) {
    if (
        props.stoneAvailable &&
        props.iceAvailable &&
        props.uraniumAvailable &&
        props.mooncoalAvailable &&
        props.ironAvailable
    ) {
        return (
            <FormControl>
                <FormLabel id="demo-radio-buttons-group-label">
                    {props.name}
                </FormLabel>
                <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    name="radio-buttons-group"
                    onChange={props.onChange}
                >
                    <FormControlLabel
                        value="Stone"
                        control={<Radio />}
                        label={"Stone | x" + props.player.stoneMulti}
                    />
                    <FormControlLabel
                        value={"Iron"}
                        control={<Radio />}
                        label={"Iron | x" + props.player.ironMulti}
                    />
                    <FormControlLabel
                        value="Ice"
                        control={<Radio />}
                        label={"Ice | x" + props.player.iceMulti}
                    />
                    <FormControlLabel
                        value="Moon Coal"
                        control={<Radio />}
                        label={"Moon Coal | x" + props.player.mooncoalMulti}
                    />
                    <FormControlLabel
                        value="Uranium"
                        control={<Radio />}
                        label={"Uranium | x" + props.player.uraniumMulti}
                    />
                </RadioGroup>
                <Button variant="contained" onClick={props.mine}>
                    Start mining
                </Button>
            </FormControl>
        );
    }
    if (
        props.stoneAvailable &&
        props.iceAvailable &&
        props.mooncoalAvailable &&
        props.ironAvailable
    ) {
        return (
            <FormControl>
                <FormLabel id="demo-radio-buttons-group-label">
                    {props.name}
                </FormLabel>
                <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    name="radio-buttons-group"
                    onChange={props.onChange}
                >
                    <FormControlLabel
                        value="Stone"
                        control={<Radio />}
                        label={"Stone | x" + props.player.stoneMulti}
                    />
                    <FormControlLabel
                        value={"Iron"}
                        control={<Radio />}
                        label={"Iron | x" + props.player.ironMulti}
                    />
                    <FormControlLabel
                        value="Ice"
                        control={<Radio />}
                        label={"Ice | x" + props.player.iceMulti}
                    />
                    <FormControlLabel
                        value="Moon Coal"
                        control={<Radio />}
                        label={"Moon Coal | x" + props.player.mooncoalMulti}
                    />
                </RadioGroup>
                <Button variant="contained" onClick={props.mine}>
                    Start mining
                </Button>
            </FormControl>
        );
    }
    if (props.stoneAvailable && props.iceAvailable && props.ironAvailable) {
        return (
            <FormControl>
                <FormLabel id="demo-radio-buttons-group-label">
                    {props.name}
                </FormLabel>
                <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    name="radio-buttons-group"
                    onChange={props.onChange}
                >
                    <FormControlLabel
                        value="Stone"
                        control={<Radio />}
                        label={"Stone | x" + props.player.stoneMulti}
                    />
                    <FormControlLabel
                        value={"Iron"}
                        control={<Radio />}
                        label={"Iron | x" + props.player.ironMulti}
                    />
                    <FormControlLabel
                        value="Ice"
                        control={<Radio />}
                        label={"Ice | x" + props.player.iceMulti}
                    />
                </RadioGroup>
                <Button variant="contained" onClick={props.mine}>
                    Start mining
                </Button>
            </FormControl>
        );
    }
    if (props.stoneAvailable && props.ironAvailable) {
        return (
            <FormControl>
                <FormLabel id="demo-radio-buttons-group-label">
                    {props.name}
                </FormLabel>
                <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    name="radio-buttons-group"
                    onChange={props.onChange}
                >
                    <FormControlLabel
                        value="Stone"
                        control={<Radio />}
                        label={"Stone | x" + props.player.stoneMulti}
                    />
                    <FormControlLabel
                        value={"Iron"}
                        control={<Radio />}
                        label={"Iron | x" + props.player.ironMulti}
                    />
                </RadioGroup>
                <Button variant="contained" onClick={props.mine}>
                    Start mining
                </Button>
            </FormControl>
        );
    }
    if (props.stoneAvailable) {
        return (
            <FormControl>
                <FormLabel id="demo-radio-buttons-group-label">
                    {props.name}
                </FormLabel>
                <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    name="radio-buttons-group"
                    onChange={props.onChange}
                >
                    <FormControlLabel
                        value="Stone"
                        control={<Radio />}
                        label={"Stone | x" + props.player.stoneMulti}
                    />
                </RadioGroup>
                <Button variant="contained" onClick={props.mine}>
                    Start mining
                </Button>
            </FormControl>
        );
    }
}
