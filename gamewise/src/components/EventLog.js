import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import { List, ListItem, ListItemText, ListSubheader } from "@mui/material";
import { useState } from "react";

const flavorEvents = [
    "Crash Landed",
    "Survived first Encounter with Moon Spiders",
    "Space is boring",
    "Maybe if I gather enough resources the moon spiders will leave me alone",
    "Just a little more and I'll be able to build something really cool",
    "I wonder if the moon spiders fear me as much as I fear them...",
    "At least I won't ever run out of potatoes to eat",
    "Tonight's feast will be potato, with a side of potato, and for dessert also potato.",
    "At least the view is fantastic!",
    "When I agreed to go to space I didn't realize there would be this much manual labor",
    "Is it just my imagination or are these moon spiders getting bigger?",
];

export default function EventLog(props) {
    const [loggedEvents, setLoggedEvents] = useState([]);
    const [count, setCount] = useState(0);
    const [eventLogInterval, setEventLogInterval] = useState(null);
    function selectEvent() {
        if (loggedEvents.length >= 5) {
            setLoggedEvents([]);
        }
        const eventChoiceIndex = Math.floor(
            Math.random() * flavorEvents.length
        );
        const eventChoice = {
            value: flavorEvents[eventChoiceIndex],
            key: count,
        };

        setLoggedEvents([...loggedEvents, eventChoice]);
        setCount(count + 1);
    }

    setInterval(selectEvent, 100000);
    const eventList = loggedEvents;
    console.log(eventList);
    const listItems = eventList.map((event) => (
        <ListItem key={event.key}>
            <ListItemText primary={event.value} />
        </ListItem>
    ));

    return (
        <Card sx={{ minWidth: 275 }} className="eventLog">
            <CardContent>
                <Typography
                    sx={{ fontSize: 14 }}
                    color="text.secondary"
                    gutterBottom
                >
                    Event Log
                </Typography>
                <List
                    sx={{
                        width: "100%",
                        position: "relative",
                        overflow: "auto",
                        maxHeight: 300,
                        "& ul": { padding: 0 },
                    }}
                >
                    {listItems}
                </List>
            </CardContent>
        </Card>
    );
}
