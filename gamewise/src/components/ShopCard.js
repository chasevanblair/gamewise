import * as React from "react";
import { styled } from "@mui/material/styles";
import ArrowForwardIosSharpIcon from "@mui/icons-material/ArrowForwardIosSharp";
import MuiAccordion from "@mui/material/Accordion";
import MuiAccordionSummary from "@mui/material/AccordionSummary";
import MuiAccordionDetails from "@mui/material/AccordionDetails";
import Typography from "@mui/material/Typography";
import ShopItem from "./ShopItem";
import { AccordionActions } from "@mui/material";
import Button from "@mui/material/Button";
import playerClass from "../codeforstuff/playerClass";

const Accordion = styled((props) => (
    <MuiAccordion disableGutters elevation={0} square {...props} />
))(({ theme }) => ({
    border: `1px solid ${theme.palette.divider}`,
    "&:not(:last-child)": {
        borderBottom: 0,
    },
    "&:before": {
        display: "none",
    },
}));

const AccordionSummary = styled((props) => (
    <MuiAccordionSummary
        expandIcon={<ArrowForwardIosSharpIcon sx={{ fontSize: "0.9rem" }} />}
        {...props}
    />
))(({ theme }) => ({
    backgroundColor:
        theme.palette.mode === "dark"
            ? "rgba(255, 255, 255, .05)"
            : "rgba(0, 0, 0, .03)",
    flexDirection: "row-reverse",
    "& .MuiAccordionSummary-expandIconWrapper.Mui-expanded": {
        transform: "rotate(90deg)",
    },
    "& .MuiAccordionSummary-content": {
        marginLeft: theme.spacing(1),
    },
}));

const AccordionDetails = styled(MuiAccordionDetails)(({ theme }) => ({
    padding: theme.spacing(2),
    borderTop: "1px solid rgba(0, 0, 0, .125)",
}));

export default function CustomizedAccordions(props) {
    const [expanded, setExpanded] = React.useState("panel1");

    const handleChange = (panel) => (event, newExpanded) => {
        setExpanded(newExpanded ? panel : false);
    };

    return (
        <div className="shopAcc">
            <Accordion
                expanded={expanded === "panel1"}
                onChange={handleChange("panel1")}
            >
                <AccordionSummary
                    aria-controls="panel1d-content"
                    id="panel1d-header"
                >
                    <Typography>Pickaxe Upgrade</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <ShopItem
                        description={[
                            "Upgrade your boring pickaxe for +1 stone mining speed",
                            "Upgrading 5 times you to mine iron.",
                        ]}
                        price={[10]}
                        type={["Stone"]}
                    />
                </AccordionDetails>
                <AccordionActions>
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={() => props.functionAdd("stone", 1, 10)}
                    >
                        Buy
                    </Button>
                </AccordionActions>
            </Accordion>
            <Accordion
                expanded={expanded === "panel2"}
                onChange={handleChange("panel2")}
            >
                <AccordionSummary
                    aria-controls="panel2d-content"
                    id="panel2d-header"
                >
                    <Typography>Mining Laser</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <ShopItem
                        description={[
                            "Build a high tech mining laser for +10 iron mining speed.",
                            "Owning 10 unlocks the ability to mine ice.",
                        ]}
                        price={[50]}
                        type={["Iron"]}
                    />
                </AccordionDetails>
                <AccordionActions>
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={() => props.functionAdd("iron", 10, 50)}
                    >
                        Buy
                    </Button>
                </AccordionActions>
            </Accordion>
            <Accordion
                expanded={expanded === "panel3"}
                onChange={handleChange("panel3")}
            >
                <AccordionSummary
                    aria-controls="panel3d-content"
                    id="panel3d-header"
                >
                    <Typography>Cryo Gathering</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <ShopItem
                        description={[
                            "Buy 10 to unlock the ability to mine moon coal!",
                            "Each unit increases ice mining speed by +20.",
                        ]}
                        price={[200]}
                        type={["Ice"]}
                    />
                </AccordionDetails>
                <AccordionActions>
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={() => props.functionAdd("ice", 50, 200)}
                    >
                        Buy
                    </Button>
                </AccordionActions>
            </Accordion>
            <Accordion
                expanded={expanded === "panel4"}
                onChange={handleChange("panel4")}
            >
                <AccordionSummary
                    aria-controls="panel4d-content"
                    id="panel4d-header"
                >
                    <Typography>Nuclear Blasting</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <ShopItem
                        description={[
                            "The Nuclear Option!",
                            "+500 mining speed for moon coal. Buy ten to unlock uranium.",
                        ]}
                        price={[500]}
                        type={["Moon Coal"]}
                    />
                </AccordionDetails>
                <AccordionActions>
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={() => props.functionAdd("mooncoal", 500, 500)}
                    >
                        Buy
                    </Button>
                </AccordionActions>
            </Accordion>
            <Accordion
                expanded={expanded === "panel5"}
                onChange={handleChange("panel5")}
            >
                <AccordionSummary
                    aria-controls="panel4d-content"
                    id="panel4d-header"
                >
                    <Typography>Carbon-Edge Drill Head</Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <ShopItem
                        description={[
                            "Ultimate upgrade to mining ability.",
                            "+1000 mining speed for uranium.",
                        ]}
                        price={[1000]}
                        type={["Uranium"]}
                    />
                </AccordionDetails>
                <AccordionActions>
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={() => props.functionAdd("uranium", 1000, 1000)}
                    >
                        Buy
                    </Button>
                </AccordionActions>
            </Accordion>
        </div>
    );
}
