import * as React from "react";

import { useState } from "react";
import MiningSelect from "./MiningSelect";
import SmithingSelect from "./SmithingSelect";
import ProgressBar from "./ProgressBar";

export default function CenterScreen(props) {
    if (props.name === "Mining") {
        if (!props.start) {
            return (
                <>
                    <MiningSelect
                        started={props.start}
                        player={props.player}
                        mine={props.mine}
                        onChange={props.onChange}
                        stoneAvailable={props.stoneAvailable}
                        ironAvailable={props.ironAvailable}
                        uraniumAvailable={props.uraniumAvailable}
                        mooncoalAvailable={props.mooncoalAvailable}
                        iceAvailable={props.iceAvailable}
                    />
                </>
            );
        } else {
            return (
                <div>
                    <MiningSelect
                        player={props.player}
                        mine={props.mine}
                        onChange={props.onChange}
                        stoneAvailable={props.stoneAvailable}
                        ironAvailable={props.ironAvailable}
                        uraniumAvailable={props.uraniumAvailable}
                        mooncoalAvailable={props.mooncoalAvailable}
                        iceAvailable={props.iceAvailable}
                    />
                    <ProgressBar />
                </div>
            );
        }
    }
    // if (props.name === "Smithing") {
    //     if (!start) {
    //         return (
    //             <>
    //                 <SmithingSelect
    //                     onClick={handleStart}
    //                     player={props.player}
    //                 />
    //             </>
    //         );
    //     } else {
    //         return (
    //             <div>
    //                 <SmithingSelect
    //                     onClick={handleStart}
    //                     player={props.player}
    //                 />
    //                 <ProgressBar />
    //             </div>
    //         );
    //     }
    // }
}
