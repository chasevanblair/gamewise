import * as React from "react";
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import InputAdornment from "@mui/material/InputAdornment";
import FormControl from "@mui/material/FormControl";
import TextField from "@mui/material/TextField";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
import Divider from "@mui/material/Divider";
import { Alert } from "@mui/material";
import { useMediaQuery } from "react-responsive";

export default function SignUp(props) {
    const [showPassword, setShowPassword] = React.useState(false);

    const handleClickShowPassword = () => setShowPassword((show) => !show);

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };
    const isDesktopOrLaptop = useMediaQuery({ query: "(min-width: 1224px)" });
    const isTabletOrMobile = useMediaQuery({ query: "(max-width: 1224px)" });

    if (props.signUpFailed) {
        return (
            //TODO make background pretty like a card almost with depth
            <Box className="loginFields">
                {document.body.classList.add("landingPage")}

                {isDesktopOrLaptop && (
                    <Box
                        sx={{
                            display: "flex",
                            width: "550px",
                            justifyContent: "center",
                            flexDirection: "column",
                            alignItems: "center",
                            background: "#282c34",
                            padding: "20px",
                            borderRadius: "10px",
                        }}
                    >
                        <Alert severity="error">Passwords do not match</Alert>
                        <TextField
                            sx={{ m: 1, width: "100%" }}
                            required
                            id="outlined-required"
                            label="Username"
                            onChange={props.onChangeUsername}
                        />
                        <FormControl
                            sx={{ m: 1, width: "100%" }}
                            variant="outlined"
                            required
                        >
                            <InputLabel htmlFor="outlined-adornment-password">
                                Password
                            </InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-password"
                                type={showPassword ? "text" : "password"}
                                onChange={props.onChangePassword}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowPassword}
                                            onMouseDown={
                                                handleMouseDownPassword
                                            }
                                            edge="end"
                                        >
                                            {showPassword ? (
                                                <VisibilityOff />
                                            ) : (
                                                <Visibility />
                                            )}
                                        </IconButton>
                                    </InputAdornment>
                                }
                                label="Password"
                            />
                        </FormControl>
                        <FormControl
                            sx={{ m: 1, width: "100%" }}
                            variant="outlined"
                            required
                        >
                            <InputLabel htmlFor="outlined-adornment-password">
                                Confirm Password
                            </InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-password"
                                type={showPassword ? "text" : "password"}
                                onChange={props.onChangeConfirmPassword}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowPassword}
                                            onMouseDown={
                                                handleMouseDownPassword
                                            }
                                            edge="end"
                                        >
                                            {showPassword ? (
                                                <VisibilityOff />
                                            ) : (
                                                <Visibility />
                                            )}
                                        </IconButton>
                                    </InputAdornment>
                                }
                                label="Password"
                            />
                        </FormControl>

                        {/* login/signup button section */}
                        <Stack
                            direction="row"
                            spacing={2}
                            divider={
                                <Divider orientation="vertical" flexItem />
                            }
                            justifyContent="space-around"
                            width={"100%"}
                            sx={{ marginTop: 3 }}
                        >
                            {/* TODO make this post to db a new user */}
                            <Button
                                variant="outlined"
                                color="secondary"
                                sx={{ width: "40%" }}
                                onClick={props.cancel}
                            >
                                Cancel
                            </Button>
                            <Button
                                variant="contained"
                                sx={{ width: "40%" }}
                                onClick={props.confirm}
                            >
                                Submit
                            </Button>
                        </Stack>
                    </Box>
                )}
                {isTabletOrMobile && (
                    <Box
                        sx={{
                            display: "flex",
                            width: "350px",
                            justifyContent: "center",
                            flexDirection: "column",
                            alignItems: "center",
                            background: "#282c34",
                            padding: "20px",
                            borderRadius: "10px",
                        }}
                    >
                        <Alert severity="error">Passwords do not match</Alert>
                        <TextField
                            sx={{ m: 1, width: "100%" }}
                            required
                            id="outlined-required"
                            label="Username"
                            onChange={props.onChangeUsername}
                        />
                        <FormControl
                            sx={{ m: 1, width: "100%" }}
                            variant="outlined"
                            required
                        >
                            <InputLabel htmlFor="outlined-adornment-password">
                                Password
                            </InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-password"
                                type={showPassword ? "text" : "password"}
                                onChange={props.onChangePassword}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowPassword}
                                            onMouseDown={
                                                handleMouseDownPassword
                                            }
                                            edge="end"
                                        >
                                            {showPassword ? (
                                                <VisibilityOff />
                                            ) : (
                                                <Visibility />
                                            )}
                                        </IconButton>
                                    </InputAdornment>
                                }
                                label="Password"
                            />
                        </FormControl>
                        <FormControl
                            sx={{ m: 1, width: "100%" }}
                            variant="outlined"
                            required
                        >
                            <InputLabel htmlFor="outlined-adornment-password">
                                Confirm Password
                            </InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-password"
                                type={showPassword ? "text" : "password"}
                                onChange={props.onChangeConfirmPassword}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowPassword}
                                            onMouseDown={
                                                handleMouseDownPassword
                                            }
                                            edge="end"
                                        >
                                            {showPassword ? (
                                                <VisibilityOff />
                                            ) : (
                                                <Visibility />
                                            )}
                                        </IconButton>
                                    </InputAdornment>
                                }
                                label="Password"
                            />
                        </FormControl>

                        {/* login/signup button section */}
                        <Stack
                            direction="row"
                            spacing={2}
                            divider={
                                <Divider orientation="vertical" flexItem />
                            }
                            justifyContent="space-around"
                            width={"100%"}
                            sx={{ marginTop: 3 }}
                        >
                            {/* TODO make this post to db a new user */}
                            <Button
                                variant="outlined"
                                color="secondary"
                                sx={{ width: "40%" }}
                                onClick={props.cancel}
                            >
                                Cancel
                            </Button>
                            <Button
                                variant="contained"
                                sx={{ width: "40%" }}
                                onClick={props.confirm}
                            >
                                Submit
                            </Button>
                        </Stack>
                    </Box>
                )}
            </Box>
        );
    } else {
        return (
            //TODO make background pretty like a card almost with depth
            <Box className="loginFields">
                {document.body.classList.add("landingPage")}

                {isDesktopOrLaptop && (
                    <Box
                        sx={{
                            display: "flex",
                            width: "550px",
                            justifyContent: "center",
                            flexDirection: "column",
                            alignItems: "center",
                            background: "#282c34",
                            padding: "20px",
                            borderRadius: "10px",
                        }}
                    >
                        <TextField
                            sx={{ m: 1, width: "100%" }}
                            required
                            id="outlined-required"
                            label="Username"
                            onChange={props.onChangeUsername}
                        />
                        <FormControl
                            sx={{ m: 1, width: "100%" }}
                            variant="outlined"
                            required
                        >
                            <InputLabel htmlFor="outlined-adornment-password">
                                Password
                            </InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-password"
                                type={showPassword ? "text" : "password"}
                                onChange={props.onChangePassword}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowPassword}
                                            onMouseDown={
                                                handleMouseDownPassword
                                            }
                                            edge="end"
                                        >
                                            {showPassword ? (
                                                <VisibilityOff />
                                            ) : (
                                                <Visibility />
                                            )}
                                        </IconButton>
                                    </InputAdornment>
                                }
                                label="Password"
                            />
                        </FormControl>
                        <FormControl
                            sx={{ m: 1, width: "100%" }}
                            variant="outlined"
                            required
                        >
                            <InputLabel htmlFor="outlined-adornment-password">
                                Confirm Password
                            </InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-password"
                                type={showPassword ? "text" : "password"}
                                onChange={props.onChangeConfirmPassword}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowPassword}
                                            onMouseDown={
                                                handleMouseDownPassword
                                            }
                                            edge="end"
                                        >
                                            {showPassword ? (
                                                <VisibilityOff />
                                            ) : (
                                                <Visibility />
                                            )}
                                        </IconButton>
                                    </InputAdornment>
                                }
                                label="Password"
                            />
                        </FormControl>

                        {/* login/signup button section */}
                        <Stack
                            direction="row"
                            spacing={2}
                            divider={
                                <Divider orientation="vertical" flexItem />
                            }
                            justifyContent="space-around"
                            width={"100%"}
                            sx={{ marginTop: 3 }}
                        >
                            {/* TODO make this post to db a new user */}
                            <Button
                                variant="outlined"
                                color="secondary"
                                sx={{ width: "40%" }}
                                onClick={props.cancel}
                            >
                                Cancel
                            </Button>
                            <Button
                                variant="contained"
                                sx={{ width: "40%" }}
                                onClick={props.confirm}
                            >
                                Submit
                            </Button>
                        </Stack>
                    </Box>
                )}
                {isTabletOrMobile && (
                    <Box
                        sx={{
                            display: "flex",
                            width: "350px",
                            justifyContent: "center",
                            flexDirection: "column",
                            alignItems: "center",
                            background: "#282c34",
                            padding: "20px",
                            borderRadius: "10px",
                        }}
                    >
                        <TextField
                            sx={{ m: 1, width: "100%" }}
                            required
                            id="outlined-required"
                            label="Username"
                            onChange={props.onChangeUsername}
                        />
                        <FormControl
                            sx={{ m: 1, width: "100%" }}
                            variant="outlined"
                            required
                        >
                            <InputLabel htmlFor="outlined-adornment-password">
                                Password
                            </InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-password"
                                type={showPassword ? "text" : "password"}
                                onChange={props.onChangePassword}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowPassword}
                                            onMouseDown={
                                                handleMouseDownPassword
                                            }
                                            edge="end"
                                        >
                                            {showPassword ? (
                                                <VisibilityOff />
                                            ) : (
                                                <Visibility />
                                            )}
                                        </IconButton>
                                    </InputAdornment>
                                }
                                label="Password"
                            />
                        </FormControl>
                        <FormControl
                            sx={{ m: 1, width: "100%" }}
                            variant="outlined"
                            required
                        >
                            <InputLabel htmlFor="outlined-adornment-password">
                                Confirm Password
                            </InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-password"
                                type={showPassword ? "text" : "password"}
                                onChange={props.onChangeConfirmPassword}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowPassword}
                                            onMouseDown={
                                                handleMouseDownPassword
                                            }
                                            edge="end"
                                        >
                                            {showPassword ? (
                                                <VisibilityOff />
                                            ) : (
                                                <Visibility />
                                            )}
                                        </IconButton>
                                    </InputAdornment>
                                }
                                label="Password"
                            />
                        </FormControl>

                        {/* login/signup button section */}
                        <Stack
                            direction="row"
                            spacing={2}
                            divider={
                                <Divider orientation="vertical" flexItem />
                            }
                            justifyContent="space-around"
                            width={"100%"}
                            sx={{ marginTop: 3 }}
                        >
                            {/* TODO make this post to db a new user */}
                            <Button
                                variant="outlined"
                                color="secondary"
                                sx={{ width: "40%" }}
                                onClick={props.cancel}
                            >
                                Cancel
                            </Button>
                            <Button
                                variant="contained"
                                sx={{ width: "40%" }}
                                onClick={props.confirm}
                            >
                                Submit
                            </Button>
                        </Stack>
                    </Box>
                )}
            </Box>
        );
    }
}
