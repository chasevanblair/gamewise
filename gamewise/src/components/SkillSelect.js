import CenterScreen from "./CenterScreen";

import * as React from "react";
import PropTypes from "prop-types";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        "aria-controls": `simple-tabpanel-${index}`,
    };
}

export default function SkillSelect(props) {
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const valueMap = {
        0: "Mining",
    };

    return (
        <Box className="skillSelect" sx={{ width: "100%" }}>
            <Box
                sx={{
                    flexGrow: 1,
                    bgcolor: "background.paper",
                    display: "flex",
                    height: 224,
                }}
            >
                <Tabs
                    orientation="vertical"
                    variant="scrollable"
                    value={value}
                    onChange={handleChange}
                    sx={{ borderRight: 1, borderColor: "divider", margin: 2 }}
                >
                    <Tab label="Mining" {...a11yProps(0)} />
                </Tabs>
                <CenterScreen
                    name={valueMap[value]}
                    className="center-screen"
                    player={props.player}
                    mine={props.mine}
                    onChange={props.onChange}
                    stoneAvailable={props.stoneAvailable}
                    ironAvailable={props.ironAvailable}
                    uraniumAvailable={props.uraniumAvailable}
                    mooncoalAvailable={props.mooncoalAvailable}
                    iceAvailable={props.iceAvailable}
                    start={props.start}
                />
            </Box>
        </Box>
    );
}
