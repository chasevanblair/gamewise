import * as React from "react";
import Box from "@mui/material/Box";
import IconButton from "@mui/material/IconButton";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import InputAdornment from "@mui/material/InputAdornment";
import FormControl from "@mui/material/FormControl";
import TextField from "@mui/material/TextField";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import Button from "@mui/material/Button";
import Stack from "@mui/material/Stack";
import Divider from "@mui/material/Divider";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import { useMediaQuery } from "react-responsive";
import { Alert } from "@mui/material";

export default function Login(props) {
    const [showPassword, setShowPassword] = React.useState(false);

    const handleClickShowPassword = () => setShowPassword((show) => !show);

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    const isDesktopOrLaptop = useMediaQuery({ query: "(min-width: 1224px)" });
    const isTabletOrMobile = useMediaQuery({ query: "(max-width: 1224px)" });
    const isPortrait = useMediaQuery({ query: "(orientation: portrait)" });

    if (props.loginFailed) {
        return (
            //TODO make background pretty like a card almost with depth
            <Box className="loginFields">
                {document.body.classList.add("landingPage")}

                {isDesktopOrLaptop && (
                    <Box
                        sx={{
                            display: "flex",
                            width: "550px",
                            justifyContent: "center",
                            flexDirection: "column",
                            alignItems: "center",
                            background: "#282c34",
                            padding: "20px",
                            borderRadius: "10px",
                        }}
                    >
                        <Alert severity="error">
                            Username/Password do not match
                        </Alert>
                        <TextField
                            sx={{ m: 1, width: "480px" }}
                            required
                            id="outlined-required"
                            label="Username"
                            onChange={props.onChangeUsername}
                        />
                        <FormControl
                            sx={{ m: 1, width: "480px" }}
                            variant="outlined"
                            required
                        >
                            <InputLabel htmlFor="outlined-adornment-password">
                                Password
                            </InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-password"
                                type={showPassword ? "text" : "password"}
                                onChange={props.onChangePassword}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowPassword}
                                            onMouseDown={
                                                handleMouseDownPassword
                                            }
                                            edge="end"
                                        >
                                            {showPassword ? (
                                                <VisibilityOff />
                                            ) : (
                                                <Visibility />
                                            )}
                                        </IconButton>
                                    </InputAdornment>
                                }
                                label="Password"
                            />
                        </FormControl>
                        <Stack
                            direction="row"
                            spacing={2}
                            divider={
                                <Divider orientation="vertical" flexItem />
                            }
                            justifyContent="space-around"
                            width={"250px"}
                            sx={{ marginTop: 3 }}
                        >
                            <Button
                                variant="contained"
                                sx={{ width: "40%" }}
                                onClick={props.onClick}
                            >
                                Login
                            </Button>
                            {/* TODO make this post to db a new user */}
                            <Button
                                variant="contained"
                                sx={{ width: "40%" }}
                                onClick={props.signUpClick}
                            >
                                Sign Up
                            </Button>
                        </Stack>
                    </Box>
                )}
                {isTabletOrMobile && (
                    <Box
                        sx={{
                            display: "flex",
                            width: "350px",
                            justifyContent: "center",
                            flexDirection: "column",
                            alignItems: "center",
                            background: "#282c34",
                            padding: "20px",
                            borderRadius: "10px",
                        }}
                    >
                        <Alert severity="error">
                            Username/Password do not match
                        </Alert>
                        <TextField
                            sx={{ m: 1, width: "280px" }}
                            required
                            id="outlined-required"
                            label="Username"
                        />
                        <FormControl
                            sx={{ m: 1, width: "280px" }}
                            variant="outlined"
                            required
                        >
                            <InputLabel htmlFor="outlined-adornment-password">
                                Password
                            </InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-password"
                                type={showPassword ? "text" : "password"}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowPassword}
                                            onMouseDown={
                                                handleMouseDownPassword
                                            }
                                            edge="end"
                                        >
                                            {showPassword ? (
                                                <VisibilityOff />
                                            ) : (
                                                <Visibility />
                                            )}
                                        </IconButton>
                                    </InputAdornment>
                                }
                                label="Password"
                            />
                        </FormControl>
                        <Stack
                            direction="column"
                            spacing={2}
                            divider={
                                <Divider orientation="vertical" flexItem />
                            }
                            justifyContent="center"
                            width={"250px"}
                            sx={{ marginTop: 3 }}
                        >
                            <Button
                                variant="contained"
                                sx={{ width: "40%" }}
                                onClick={props.onClick}
                            >
                                Login
                            </Button>
                            {/* TODO make this post to db a new user */}
                            <Button
                                variant="contained"
                                sx={{ width: "40%" }}
                                onClick={props.signUpClick}
                            >
                                Sign Up
                            </Button>
                        </Stack>
                    </Box>
                )}
            </Box>
        );
    }

    return (
        //TODO make background pretty like a card almost with depth
        <Box className="loginFields">
            {document.body.classList.add("landingPage")}

            {isDesktopOrLaptop && (
                <Box
                    sx={{
                        display: "flex",
                        width: "550px",
                        justifyContent: "center",
                        flexDirection: "column",
                        alignItems: "center",
                        background: "#282c34",
                        padding: "20px",
                        borderRadius: "10px",
                    }}
                >
                    <TextField
                        sx={{ m: 1, width: "480px" }}
                        required
                        id="outlined-required"
                        label="Username"
                        onChange={props.onChangeUsername}
                    />
                    <FormControl
                        sx={{ m: 1, width: "480px" }}
                        variant="outlined"
                        required
                    >
                        <InputLabel htmlFor="outlined-adornment-password">
                            Password
                        </InputLabel>
                        <OutlinedInput
                            id="outlined-adornment-password"
                            type={showPassword ? "text" : "password"}
                            onChange={props.onChangePassword}
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={handleClickShowPassword}
                                        onMouseDown={handleMouseDownPassword}
                                        edge="end"
                                    >
                                        {showPassword ? (
                                            <VisibilityOff />
                                        ) : (
                                            <Visibility />
                                        )}
                                    </IconButton>
                                </InputAdornment>
                            }
                            label="Password"
                        />
                    </FormControl>
                    <Stack
                        direction="row"
                        spacing={2}
                        divider={<Divider orientation="vertical" flexItem />}
                        justifyContent="space-around"
                        width={"250px"}
                        sx={{ marginTop: 3 }}
                    >
                        <Button
                            variant="contained"
                            sx={{ width: "40%" }}
                            onClick={props.onClick}
                        >
                            Login
                        </Button>
                        {/* TODO make this post to db a new user */}
                        <Button
                            variant="contained"
                            sx={{ width: "40%" }}
                            onClick={props.signUpClick}
                        >
                            Sign Up
                        </Button>
                    </Stack>
                </Box>
            )}
            {isTabletOrMobile && (
                <Box
                    sx={{
                        display: "flex",
                        width: "350px",
                        justifyContent: "center",
                        flexDirection: "column",
                        alignItems: "center",
                        background: "#282c34",
                        padding: "20px",
                        borderRadius: "10px",
                    }}
                >
                    <TextField
                        sx={{ m: 1, width: "280px" }}
                        required
                        id="outlined-required"
                        label="Username"
                    />
                    <FormControl
                        sx={{ m: 1, width: "280px" }}
                        variant="outlined"
                        required
                    >
                        <InputLabel htmlFor="outlined-adornment-password">
                            Password
                        </InputLabel>
                        <OutlinedInput
                            id="outlined-adornment-password"
                            type={showPassword ? "text" : "password"}
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={handleClickShowPassword}
                                        onMouseDown={handleMouseDownPassword}
                                        edge="end"
                                    >
                                        {showPassword ? (
                                            <VisibilityOff />
                                        ) : (
                                            <Visibility />
                                        )}
                                    </IconButton>
                                </InputAdornment>
                            }
                            label="Password"
                        />
                    </FormControl>
                    <Stack
                        direction="column"
                        spacing={2}
                        divider={<Divider orientation="vertical" flexItem />}
                        justifyContent="center"
                        width={"250px"}
                        sx={{ marginTop: 3 }}
                    >
                        <Button
                            variant="contained"
                            sx={{ width: "40%" }}
                            onClick={props.onClick}
                        >
                            Login
                        </Button>
                        {/* TODO make this post to db a new user */}
                        <Button
                            variant="contained"
                            sx={{ width: "40%" }}
                            onClick={props.signUpClick}
                        >
                            Sign Up
                        </Button>
                    </Stack>
                </Box>
            )}
        </Box>
    );
}
