import PlayerStats from "./PlayerStats";
import SkillSelect from "./SkillSelect";
import EventLog from "./EventLog";
import ShopCard from "./ShopCard";
import { useCallback, useState } from "react";
import playerClass from "../codeforstuff/playerClass";

export default function Game(props) {
    // This is be the parent component for all of the game UI
    const defaultPlayer = {
        name: "Chase",
        Iron: 0,
        Stone: 0,
        Uranium: 0,
        "Moon Coal": 0,
        Ice: 0,
    };

    console.log(
        "The player data with which we are trying to create a player: ",
        props.playerData
    );

    console.log("expected stone value", props.playerData.stone);

    const [player, setPlayer] = useState(props.playerData);
    const [ironAvailable, setIronAvailable] = useState(player.stoneMulti >= 5);
    const [mooncoalAvailable, setMooncoalAvailable] = useState(
        player.iceMulti >= 501
    );
    const [iceAvailable, setIceAvailable] = useState(player.ironMulti >= 101);
    const [uraniumAvailable, setUraniumAvailable] = useState(
        player.mooncoalMulti >= 5001
    );
    const [stoneAvailable, setStoneAvailable] = useState(true);
    const [resourceGather, setResourceGather] = useState(undefined);

    const [currentResourceSelected, setCurrentResourceSelected] =
        useState(undefined);
    const [start, setStart] = useState(false);

    console.log("created player is: ", player);

    function handleMiningSelectChange(event) {
        setCurrentResourceSelected(event.target.value);
    }

    function increasePMult(resource, amount, cost) {
        clearInterval(resourceGather);
        setStart(false);
        if (resource === "stone" && player.stone >= cost) {
            player.stoneMulti += amount;
            player.stone -= cost;
            if (player.stoneMulti === 5) {
                player.iron = 0;
                setIronAvailable(true);
            }
        }
        if (resource === "iron" && player.iron >= cost) {
            player.ironMulti += amount;
            player.iron -= cost;
            if (player.ironMulti === 101) {
                player.ice = 0;
                setIceAvailable(true);
            }
        }
        if (resource === "ice" && player.ice >= cost) {
            player.iceMulti += amount;
            player.ice -= cost;
            if (player.iceMulti === 501) {
                player.mooncoal = 0;
                setMooncoalAvailable(true);
            }
        }
        if (resource === "mooncoal" && player.mooncoal >= cost) {
            player.mooncoalMulti += amount;
            player.mooncoal -= cost;
            if (player.mooncoalMulti === 5001) {
                player.uranium = 0;
                setUraniumAvailable(true);
            }
        }
        if (resource === "uranium" && player.uranium >= cost) {
            player.uraniumMulti += amount;
            player.uranium -= cost;
        }
        console.log(player.stoneMulti);
        setPlayer({ ...player });
        savePlayer();
        handleMiningResource();
    }

    async function savePlayer() {
        const BASE_URL = "";
        await fetch(`${BASE_URL}/gamewise/api/auth/save`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                username: props.username,
                resources: player,
            }),
        })
            .then((response) => response.json())
            .then((response) => {
                if (response["success"] === true) {
                    console.log("Save succeeded");
                } else {
                    console.log("player save failed");
                }
            });
    }

    function handleMiningResource() {
        const selectedResource = currentResourceSelected;
        setStart(true);
        console.log("Function Called");
        if (resourceGather) {
            console.log("Interval Cleared", resourceGather);
            clearInterval(resourceGather);
        }
        const currentResourceGather = setInterval(() => {
            // player[selectedResource] += 1;
            if (selectedResource === "Iron") {
                player.iron += 1 * player.ironMulti;
                if (player.iron >= 0) {
                    setIronAvailable(true);
                }
            }
            if (selectedResource === "Stone") {
                player.stone += 1 * player.stoneMulti;
            }
            if (selectedResource === "Uranium") {
                if (player.uranium >= 0) {
                    setUraniumAvailable(true);
                }
                player.uranium += 1 * player.uraniumMulti;
            }
            if (selectedResource === "Moon Coal") {
                if (player.mooncoal >= 0) {
                    setMooncoalAvailable(true);
                }
                player.mooncoal += 1 * player.mooncoalMulti;
            }
            if (selectedResource === "Ice") {
                if (player.ice >= 0) {
                    setIceAvailable(true);
                }
                player.ice += 1 * player.iceMulti;
            }
            // console.log(player);
            setPlayer({ ...player });
            savePlayer();
        }, 1000);

        setResourceGather(currentResourceGather);

        console.log("Interval Started", resourceGather);
    }

    // function handleMiningResource(event) {
    //     console.log(event.target.value);
    //     const resource = event.target.value;
    //     const resourceGather = setInterval(() => {
    //         newPlayer[resource] += 1;
    //         console.log(newPlayer);
    //         setPlayer(newPlayer);
    //     }, 1000);

    // props.player[resource] += 1;
    // console.log(props.player);

    // console.log(newPlayer.name);

    return (
        <>
            {document.body.classList.remove("landingPage")}
            <div className="statHolder">
                <ShopCard player={player} functionAdd={increasePMult} />
                <PlayerStats
                    player={player}
                    stoneAvailable={stoneAvailable}
                    ironAvailable={ironAvailable}
                    uraniumAvailable={uraniumAvailable}
                    mooncoalAvailable={mooncoalAvailable}
                    iceAvailable={iceAvailable}
                />
            </div>
            <SkillSelect
                player={player}
                mine={handleMiningResource}
                onChange={handleMiningSelectChange}
                stoneAvailable={stoneAvailable}
                ironAvailable={ironAvailable}
                uraniumAvailable={uraniumAvailable}
                mooncoalAvailable={mooncoalAvailable}
                iceAvailable={iceAvailable}
                start={start}
            />
        </>
    );
}
