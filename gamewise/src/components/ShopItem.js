import * as React from "react";
import Box from "@mui/material/Box";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";

const card = <React.Fragment></React.Fragment>;

//props for upgrade name and data
/*
    description = [1stline, 2ndline, 3rdline, ...]
    price = [1st price, 2nd price, ...]
    type = [resource of first price, resource of 2nd price, ...]

    example:
        description = ["buy mining laser", "Owning 10 unlocks ability to mine ice"]
        price = [200, 200]
        type = ["Stone", "Iron"]
*/

export default function OutlinedCard({description, price, type}) {
    return (
        <Box sx={{ minWidth: 275 }}>
            <Card variant="outlined">
                <CardContent>
                    <Typography variant="h5" component="div">
                        {description.map((line) => {
                            return(
                            <>
                                <span>{line}</span>
                                <br />
                            </>)
                        })}
                        <br/>
                        {/* needs to do price[0] type[0] price[1] type[1] ... */}
                        Cost: {price} {type}
                    </Typography>
                </CardContent>
            </Card>
        </Box>
    );
}
