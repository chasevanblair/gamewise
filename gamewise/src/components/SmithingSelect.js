import Radio from "@mui/material/Radio";
import RadioGroup from "@mui/material/RadioGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";
import FormLabel from "@mui/material/FormLabel";
import { Button } from "@mui/material";

export default function SmithingSelect(props) {
    return (
        <>
            <FormControl>
                <FormLabel id="demo-radio-buttons-group-label">
                    {props.name}
                </FormLabel>
                <RadioGroup
                    aria-labelledby="demo-radio-buttons-group-label"
                    defaultValue="Refine Iron"
                    name="radio-buttons-group"
                >
                    <FormControlLabel
                        value="Refine Iron"
                        control={<Radio />}
                        label="Refine Iron"
                    />
                    <FormControlLabel
                        value="Refine Stone"
                        control={<Radio />}
                        label="Refine Stone"
                    />
                    <FormControlLabel
                        value="Refine Uranium"
                        control={<Radio />}
                        label="Refine Uranium"
                    />
                    <FormControlLabel
                        value="Refine Moon Coal"
                        control={<Radio />}
                        label="Refine Moon Coal"
                    />
                    <FormControlLabel
                        value="Refine Ice"
                        control={<Radio />}
                        label="Refine Ice"
                    />
                </RadioGroup>
                <Button variant="contained" onClick={props.onClick}>
                    Start smithing
                </Button>
            </FormControl>
        </>
    );
}
