//TODO: json saving of player data -- JSON saving is here, but JSON file needs to be saved to the mongo for it to be effective
//TODO: implement game loop

//resource classes are to be made visible in the interface as they become relevant, and added to the player character immediately upon creation in this file.
class resource {
    constructor(amount = -1) {
        //amount set to -1 to create a check for visibility
        this.amount = amount;
    }
    addAmount(amount) {
        this.amount += amount;
    }
    setAmount(amount) {
        this.amount = amount;
    }
}
class Stone extends resource {
    constructor(amount) {
        super(0);
    }
}
class Iron extends resource {
    constructor(amount) {
        super(amount);
    }
}
class Uranium extends resource {
    constructor(amount) {
        super(amount);
    }
}
class MoonCoal extends resource {
    constructor(amount) {
        super(amount);
    }
}
class Ice extends resource {
    constructor(amount) {
        super(amount);
    }
}

export default class playerClass {
    //constructor for player character, allow player to define their name

    constructor(
        stone,
        iron,
        ice,
        mooncoal,
        uranium,
        mult_stone = 1,
        mult_iron = 1,
        mult_uranium = 1,
        mult_moonCoal = 1,
        mult_ice = 1
    ) {
        this.stone = new Stone(stone);
        this.iron = new Iron(iron);
        this.ice = new Ice(ice);
        this.mooncoal = new MoonCoal(mooncoal);
        this.uranium = new Uranium(uranium);

        //multipliers for resource gains. these will increase the amount a user gets when a certain resource is mined, and be determined by upgrades a user owns.
        this.stoneMulti = mult_stone;
        this.ironMulti = mult_iron;
        this.uraniumMulti = mult_uranium;
        this.mooncoalMulti = mult_moonCoal;
        this.iceMulti = mult_ice;
    }

    saveData(player) {
        //allows data to be saved from player class
        const pDATA = JSON.stringify(player);
        return pDATA;
    }

    increaseMulti(amount, resource) {
        if (resource == "stone") {
            this.stoneMulti += amount;
        }
        return;
    }
}
// const p = new Player();
// p.ice.setAmount(4)
// const result = JSON.stringify(p);
// p = JSON.parse(result) //this is the line that allows for player class data to be set to saved data from json
// console.log(p.ice.amount)
