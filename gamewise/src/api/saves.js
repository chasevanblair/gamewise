const express = require("express");
var passport = require("passport");
require("../config/passport")(passport);

//test access declarations can be removed
//testing mongoose schema use
const testdb = require("../../example");
//

const router = express.Router();
const Saves = require("../models/save");

router.get(
    "/saves",
    passport.authenticate("jwt", { session: false }),
    function (req, res) {
        var token = getToken(req.headers);
        if (token) {
            Saves.findOne({ name: req.body.name })
                .then((save) => res.json(save))
                .catch((err) =>
                    res.status(404).json({ noUserFound: "No user found" })
                );
        } else {
            return res
                .status(403)
                .send({ success: false, msg: "Unauthorized. " });
        }
    }
);

router.get("/feedback", function (req, res) {
    res.send(req.rawHeaders);
});

router.post(
    "/saves",
    passport.authenticate("jwt", { session: false }),
    function (req, res) {
        var token = getToken(req.headers);
        if (token) {
            Saves.create(req.body)
                .then((user) => res.json({ msg: "User created successfully" }))
                .catch((err) =>
                    res.status(400).json({ error: "Unable to create user" })
                );
        } else {
            return res
                .status(403)
                .send({ success: false, msg: "Unauthorized." });
        }
    }
);

router.put(
    "/saves",
    passport.authenticate("jwt", { session: false }),
    function (req, res) {
        var token = getToken(req.headers);
        if (token) {
            Saves.updateOne({ name: req.body.name }, req.body)
                .then((save) => res.json({ msg: "Updated successfuly" }))
                .catch((err) =>
                    res
                        .status(400)
                        .json({ error: "Unable to update the Database" })
                );
        } else {
            return res
                .status(403)
                .send({ success: false, msg: "Unauthorized." });
        }
    }
);

router.delete(
    "/saves",
    passport.authenticate("jwt", { session: false }),
    function (req, res) {
        var token = getToken(req.headers);
        if (token) {
            Saves.deleteOne({ name: req.params.name });
        } else {
            return res
                .status(403)
                .send({ success: false, msg: "Unauthorized." });
        }
    }
);

//testing api
router.get("/testdb", (req, res) => {
    //testing mongoose schema use
    testdb.saveData();
    testdb.loadData();
    res.status(200).send();
});

//

const getToken = function (headers) {
    if (headers && headers.authorization) {
        console.log(headers.authorization);
        var parted = headers.authorization.split(" ");
        if (parted.length === 2) {
            return parted[1];
        } else {
            return null;
        }
    } else {
        return null;
    }
};

module.exports = router;
