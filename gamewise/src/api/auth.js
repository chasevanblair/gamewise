var mongoose = require("mongoose");
var passport = require("passport");
var settings = require("../config/settings");
require("../config/passport")(passport);
var express = require("express");
var jwt = require("jsonwebtoken");
var router = express.Router();
var User = require("../models/user");

router.post("/register", function (req, res) {
    if (!req.body.username || !req.body.password) {
        res.json({ success: false, msg: "Please pass username and password." });
    } else {
        var newUser = new User({
            username: req.body.username,
            password: req.body.password,
        });
        // save the user
        newUser.save(function (err) {
            if (err) {
                return res.json({
                    success: false,
                    msg: "Username already exists.",
                });
            }
            res.json({ success: true, msg: "Successful created new user." });
        });
        console.log("register api was hit");
    }
});

router.post("/login", function (req, res) {
    try {
        User.findOne(
            {
                username: req.body.username,
            },
            function (err, user) {
                if (err) throw err;

                if (!user) {
                    res.status(401).send({
                        success: false,
                        msg: "Authentication failed. User not found.",
                    });
                } else {
                    // check if password matches
                    user.comparePassword(
                        req.body.password,
                        function (err, isMatch) {
                            if (isMatch && !err) {
                                // if user is found and password is right create a token
                                var token = jwt.sign(
                                    user.toJSON(),
                                    settings.secret
                                );
                                // return the information including token as JSON
                                res.json({
                                    success: true,
                                    token: "JWT " + token,
                                    playerData: user.getSaveData(),
                                });
                            } else {
                                res.status(401).send({
                                    success: false,
                                    msg: "Authentication failed. Wrong password.",
                                });
                            }
                        }
                    );
                }
            }
        );
    } catch (err) {
        console.log(err);
    }
});

router.post("/save", function (req, res) {
    let resources = req.body.resources;
    let newResources = {};
    for (let r in resources) {
        if (!r.includes("Multi")) {
            newResources[r] = resources[r];
        } else {
            newResources[r] = resources[r];
        }
    }
    console.log("new resources", newResources);
    try {
        User.findOneAndUpdate(
            {
                username: req.body.username,
            },
            {
                resources: newResources,
            },
            function (err) {
                if (err) {
                    res.status(401).send({
                        success: false,
                        msg: err,
                    });
                    return;
                }
                res.status(200).send({
                    success: true,
                    msg: "found user",
                });
            }
        );
    } catch (err) {
        console.log(err);
    }
});

module.exports = router;
