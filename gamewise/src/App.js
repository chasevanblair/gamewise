import { useState } from "react";
import "./App.css";
import Login from "./components/Login";
import Header from "./components/Header";
import "@fontsource/roboto/300.css";
import "@fontsource/roboto/400.css";
import "@fontsource/roboto/500.css";
import "@fontsource/roboto/700.css";
import Game from "./components/Game";
import EventLog from "./components/EventLog";
import { ThemeProvider, createTheme } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import SignUp from "./components/SignUp";
import playerClass from "./codeforstuff/playerClass";
const darkTheme = createTheme({
    palette: {
        mode: "dark",
    },
});

function App() {
    const [loggedIn, setLoggedIn] = useState(false);
    const [signUp, setSignUp] = useState(false);
    const [loginFailed, setLoginFailed] = useState(false);
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [signUpFailed, setSignUpFailed] = useState(false);
    const [playerData, setPlayerData] = useState({});
    function handleUsernameChange(event) {
        setUsername(event.target.value);
    }

    function handlePasswordChange(event) {
        setPassword(event.target.value);
    }

    function handleConfirmPasswordChange(event) {
        setConfirmPassword(event.target.value);
    }

    async function login() {
        //check db for user
        //pass credentials through post request
        const BASE_URL = "";
        await fetch(`${BASE_URL}/gamewise/api/auth/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({
                name: "Leo",
                username: username,
                password: password,
            }),
        })
            .then((response) => response.json())
            .then((response) => {
                if (response["success"] === true) {
                    console.log(response.playerData);
                    setPlayerData(response.playerData);
                    setLoggedIn(true);
                    setLoginFailed(false);
                } else {
                    setLoginFailed(true);
                    console.log("failed");
                }
            });
    }
    //if match then login
    //else stay on login screen but post failure warning

    async function logout() {
        //This should also create a save of the current players data
        setLoggedIn(false);
        setSignUp(false);
        setLoginFailed(false);
    }

    function handleSignUpClick() {
        setSignUp(true);
    }

    async function handleConfirmSignUpClick() {
        //Here is where we need to save the new users data and create a record for them in the db
        const BASE_URL = "";

        if (password === confirmPassword && password !== "") {
            await fetch(`${BASE_URL}/gamewise/api/auth/register`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    username: username,
                    password: password,
                }),
            })
                .then((response) => response.json())
                .then((response) => {
                    if (response["success"] === true) {
                        setLoggedIn(true);
                        setPlayerData({
                            moonCoal: -1,
                            stone: 0,
                            iron: 0,
                            ice: 0,
                            uranium: 0,
                            stoneMulti: 1,
                            ironMulti: 1,
                            uraniumMulti: 1,
                            mooncoalMulti: 1,
                            iceMulti: 1,
                        });
                        console.log("new user created");
                    }
                })
                .then((response) => console.log(JSON.stringify(response)));
        } else {
            console.log("failed");
            setSignUpFailed(true);
        }
    }

    if (!loggedIn && signUp) {
        return (
            <ThemeProvider theme={darkTheme}>
                <div className="App">
                    <CssBaseline />
                    <Header loggedIn={loggedIn} />
                    <SignUp
                        cancel={() => setSignUp(!signUp)}
                        confirm={handleConfirmSignUpClick}
                        onChangePassword={handlePasswordChange}
                        onChangeUsername={handleUsernameChange}
                        onChangeConfirmPassword={handleConfirmPasswordChange}
                        signUpFailed={signUpFailed}
                    />
                </div>
            </ThemeProvider>
        );

        //if logged in show the game
    } else if (!loggedIn) {
        //render login page
        //if not logged in show the login page
        return (
            <ThemeProvider theme={darkTheme}>
                <div className="App">
                    <CssBaseline />
                    <Header loggedIn={loggedIn} />
                    <Login
                        onClick={login}
                        signUpClick={handleSignUpClick}
                        loginFailed={loginFailed}
                        onChangeUsername={handleUsernameChange}
                        onChangePassword={handlePasswordChange}
                    />
                </div>
            </ThemeProvider>
        );
    } else {
        //render main game screen
        //get from saves

        return (
            <ThemeProvider theme={darkTheme}>
                <CssBaseline />
                <div className="App">
                    <Header loggedIn={loggedIn} logout={logout} />
                    <Game playerData={playerData} username={username} />
                    <EventLog />
                </div>
            </ThemeProvider>
        );
    }
}

export default App;
