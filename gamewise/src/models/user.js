var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var bcrypt = require("bcrypt-nodejs");

var UserSchema = new Schema({
    username: {
        type: String,
        unique: true,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    resources: {
        stone: { type: Number, default: 0 },
        iron: { type: Number, default: -1 },
        ice: { type: Number, default: -1 },
        moonCoal: { type: Number, default: -1 },
        uranium: { type: Number, default: -1 },
        stoneMulti: { type: Number, default: 1 },
        ironMulti: { type: Number, default: 1 },
        iceMulti: { type: Number, default: 1 },
        mooncoalMulti: { type: Number, default: 1 },
        uraniumMulti: { type: Number, default: 1 },
    },
});

UserSchema.pre("save", function (next) {
    var user = this;
    if (this.isModified("password") || this.isNew) {
        bcrypt.genSalt(10, function (err, salt) {
            if (err) {
                return next(err);
            }
            bcrypt.hash(user.password, salt, null, function (err, hash) {
                if (err) {
                    return next(err);
                }
                user.password = hash;
                next();
            });
        });
    } else {
        return next();
    }
});

UserSchema.methods.comparePassword = function (passw, cb) {
    bcrypt.compare(passw, this.password, function (err, isMatch) {
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    });
};

UserSchema.methods.getSaveData = function () {
    return this.resources;
};

module.exports = mongoose.model("User", UserSchema);
